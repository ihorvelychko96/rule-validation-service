package com.velychko.rvs.validation.controller;

import com.velychko.rvs.validation.model.Rule;
import com.velychko.rvs.validation.model.ValidationReport;
import com.velychko.rvs.validation.service.ValidationService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rule")
public class ValidationController {

    private ValidationService validationService;

    public ValidationController(ValidationService validationService) {
        this.validationService = validationService;
    }

    /**
     * Main endpoint for rules validation service. Consumes json file with rules.
     * Validation perform using {@link ValidationService}
     *
     * @param rule - string with json.
     * @param type - parameter which specify type of rule.
     * @return {@link ValidationReport}
     */
    @PostMapping(path = "/validate/{type}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ValidationReport> validateRule(@RequestBody String rule, @PathVariable Rule type) {
        ValidationReport report = validationService.validate(rule, type);
        return ResponseEntity.ok(report);
    }
}
