package com.velychko.rvs.validation.service;

import com.velychko.rvs.validation.model.Rule;
import com.velychko.rvs.validation.model.ValidationReport;

public interface ValidationService {

    /**
     * Main method of service.
     * Method returns {@link ValidationReport} which regarding to result can
     * consist of only boolean value if validation performed successful
     * or consist of boolean and {@link java.util.List} of errors which occurred
     * during validation process.
     * @param rule {@link Rule}
     * @return {@link ValidationReport}
     */
    ValidationReport validate(String rule, Rule type);
}
