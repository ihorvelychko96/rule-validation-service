package com.velychko.rvs.validation.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 * Class which represents response after validation
 */

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ValidationReport {

    /**
     * If validation ended up with success then assign to true else false.
     */
    private boolean success;

    /**
     * Represents array of errors happened during validation. If validation end
     * with success then list is empty.
     */
    private List<String> errors = new ArrayList<>();

}
