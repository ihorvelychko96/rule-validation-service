package com.velychko.rvs.validation.model;

public enum Rule {

    FILE("file", "file-validation-schema.json"),
    METHODOLOGY("methodology", "methodology-validation-schema.json");

    private String type;
    private String schemaName;

    Rule(String type, String schemaName) {
        this.type = type;
        this.schemaName = schemaName;
    }

    public String getType() { return type; }
    public String getSchemaName() { return schemaName; }
}
