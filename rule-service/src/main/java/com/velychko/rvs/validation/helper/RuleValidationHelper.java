package com.velychko.rvs.validation.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.velychko.rvs.validation.model.Rule;
import java.io.IOException;
import java.io.InputStream;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class RuleValidationHelper {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static InputStream loadValidationSchema(Rule type) {
        if (type == null) {
            throw new IllegalArgumentException("Rule type not specified!");
        }
        String schemaFileName = type.getSchemaName();
        return RuleValidationHelper.class.getClassLoader().getResourceAsStream(schemaFileName);
    }

    public static JsonNode createJsonNode(InputStream schemaFile) throws IOException {
        if (schemaFile == null) {
            throw new IllegalArgumentException("Schema file doesn't exist");
        }
        JsonNode schemaNode;
        schemaNode = OBJECT_MAPPER.readTree(schemaFile);
        log.info("Created JsonNode for schema from file");
        return schemaNode;
    }

    public static JsonNode createJsonNode(String rule) throws JsonProcessingException {
        if (rule == null) {
            throw new IllegalArgumentException("Rule string doesn't specified!");
        }
        JsonNode ruleNode;
        ruleNode = OBJECT_MAPPER.readTree(rule);
        log.info("Created JsonNode for rule from string");
        return ruleNode;
    }
}
