package com.velychko.rvs.validation.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.velychko.rvs.validation.model.Rule;
import com.velychko.rvs.validation.model.ValidationReport;
import com.velychko.rvs.validation.service.ValidationService;
import com.velychko.rvs.validation.helper.RuleValidationHelper;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ValidationServiceImpl implements ValidationService {

    /**
     * Main method of service. Method consume string object and convert
     * its to {@link JsonNode}. Then load schema file and also convert its to
     * {@link JsonNode}. Validation logic placed in
     * {@link ValidationServiceImpl#processValidation(JsonNode, JsonNode)} method.
     * Method returns {@link ValidationReport} which regarding to result can
     * consist of only boolean value if validation performed successful
     * or consist of boolean and {@link java.util.List} of errors which occurred
     * during validation process.
     * @param rule {@link Rule}
     * @return {@link ValidationReport}
     */
    public ValidationReport validate(String rule, Rule type) {
        ValidationReport validationReport = new ValidationReport();
        ProcessingReport report;
        try {
            JsonNode ruleNode = RuleValidationHelper.createJsonNode(rule);
            JsonNode schemaNode = RuleValidationHelper.createJsonNode(RuleValidationHelper.loadValidationSchema(type));
            report = processValidation(ruleNode, schemaNode);
        } catch (IOException | ProcessingException | IllegalArgumentException e) {
            log.error("Error when processing validation", e);
            validationReport.getErrors().add("Error when processing validation - " + e);
            return validationReport;
        }

        if (report.isSuccess()) {
            validationReport.setSuccess(true);
        } else {
            report.forEach( errorMessage -> {
                validationReport.getErrors().add(errorMessage.getMessage());
            });
        }
        return validationReport;
    }

    /**
     * Method perform validation using library -
     * <a href="https://www.github.com/java-json-tools/json-schema-validator">json-schema-validator</a>.
     * @param ruleNode represents rule.
     * @param schemaNode represents schema.
     * @return {@link ProcessingReport}.
     */
    private ProcessingReport processValidation(JsonNode ruleNode, JsonNode schemaNode) throws ProcessingException {
        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonSchema schema = factory.getJsonSchema(schemaNode);

        log.info("Processing validation of json schema...");
        ProcessingReport report = schema.validate(ruleNode);
        log.info("Validation ended up with {}", report.isSuccess() ? "success" : "failure");
        return report;
    }

}
