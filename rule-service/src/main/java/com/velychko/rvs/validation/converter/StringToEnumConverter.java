package com.velychko.rvs.validation.converter;

import com.velychko.rvs.validation.model.Rule;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToEnumConverter implements Converter<String, Rule> {

    @Override
    public Rule convert(String source) {
        return Rule.valueOf(source.toUpperCase());
    }
}
