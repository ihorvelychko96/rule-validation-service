package com.velychko.rvs.validation.service;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.velychko.rvs.validation.helper.RuleValidationHelper;
import com.velychko.rvs.validation.model.Rule;
import com.velychko.rvs.validation.model.ValidationReport;
import com.velychko.rvs.validation.service.impl.ValidationServiceImpl;
import java.io.InputStream;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ValidationServiceTest {

    @Tested private ValidationServiceImpl validationService;

    @Mocked private RuleValidationHelper helper;
    @Mocked private JsonSchemaFactory factory;
    @Mocked private JsonSchema schema;
    @Mocked private JsonNode jsonNode;
    @Mocked private ProcessingReport report;

    @DisplayName("Null rule string")
    @Test
    public void testNullRule() throws Exception {

        new Expectations(){{
            RuleValidationHelper.createJsonNode((String) null); result = new IllegalArgumentException();
        }};

        ValidationReport report = validationService.validate(null, Rule.METHODOLOGY);

        assertAll(
            () -> assertTrue(report.getErrors().size() > 0),
            () -> assertFalse(report.isSuccess())
        );

    }

    @DisplayName("Null rule type")
    @Test
    public void testNullType() throws Exception {

        new Expectations(){{
            RuleValidationHelper.loadValidationSchema(null); result = new IllegalArgumentException();
        }};

        ValidationReport report = validationService.validate("{}", null);

        assertAll(
            () -> assertTrue(report.getErrors().size() > 0),
            () -> assertFalse(report.isSuccess())
        );
    }

    @DisplayName("ProcessingException during validation")
    @Test
    public void testProcessingException() throws Exception {

        new Expectations() {{
            schema.validate(withInstanceOf(JsonNode.class)); result = new ProcessingException();
        }};

        ValidationReport report = validationService.validate("{}", Rule.METHODOLOGY);

        assertAll(
            () -> assertTrue(report.getErrors().size() > 0),
            () -> assertFalse(report.isSuccess())
        );
    }

    @DisplayName("Success validation")
    @Test
    public void testSuccessValidation() throws Exception {

        new Expectations() {{
            RuleValidationHelper.createJsonNode(anyString); result = jsonNode;
            RuleValidationHelper.createJsonNode(withInstanceOf(InputStream.class)); result = jsonNode;
            schema.validate(withInstanceOf(JsonNode.class)); result = report;
            report.isSuccess(); result = true;
        }};

        ValidationReport validationReport = validationService.validate("{}", Rule.FILE);

        assertAll(
            () -> assertFalse(validationReport.getErrors().size() > 0),
            () -> assertTrue(validationReport.isSuccess())
        );
    }

    @DisplayName("Failed validation")
    @Test
    public void testFailValidation() throws Exception {

        new Expectations(helper) {{
            RuleValidationHelper.createJsonNode(anyString); result = jsonNode;
            RuleValidationHelper.loadValidationSchema(Rule.FILE); result = "";
            RuleValidationHelper.createJsonNode(withInstanceOf(InputStream.class)); result = jsonNode;
            schema.validate(withInstanceOf(JsonNode.class)); result = report;
            report.isSuccess(); result = false;
        }};

        ValidationReport validationReport = validationService.validate("{wrong}", Rule.FILE);

        assertAll(
            //cannot check it because mocked ProcessingReport doesn't record ProcessingMessage
//            () -> assertTrue(validationReport.getErrors().size() > 0),
            () -> assertFalse(validationReport.isSuccess())
        );

    }



}
