package com.velychko.rvs.validation.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


import com.velychko.rvs.validation.model.ValidationReport;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class TestUtil {

    /**
     * Helper method which loads files from test resource directory.
     *
     * @param fileName - name of file with extension.
     * @return string representation of file.
     */
    public static String loadFile(String fileName) {
        File file = new File("src/test/resources/" + fileName);
        String rule;
        try {
            rule = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new RuntimeException("Error when loading resources file!", e);
        }
        return rule;
    }

    /**
     * Performs assertion on {@link ValidationReport} object.
     * @param report
     * @param expectedResult
     */
    public static void assertValidationReport(ValidationReport report, boolean expectedResult) {
        if (expectedResult) {
            assertAll(
                () -> assertTrue(report.isSuccess(), "Success should be true"),
                () -> assertFalse(report.getErrors().size() > 0, "Errors list should be empty")
            );
        } else {
            assertAll(
                () -> assertFalse(report.isSuccess(), "Success should be false"),
                () -> assertTrue(report.getErrors().size() > 0, "Errors list should be not empty")
            );
        }
    }
}
