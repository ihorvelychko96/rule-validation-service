package com.velychko.rvs.validation.controller;

import com.velychko.rvs.validation.model.ValidationReport;
import com.velychko.rvs.validation.util.TestUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ValidationControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @DisplayName("Empty file")
    @Test
    public void testEmptyFile() {
        executeControllerTest("emptyFile.json", "file", false);
    }

    @DisplayName("Correct file type rule")
    @Test
    public void testValidationFileType() {
        executeControllerTest("medicalClaim.json", "file", true);
    }

    @DisplayName("Correct methodology type rule")
    @Test
    public void testValidationMethodologyType() {
        executeControllerTest("meg9.34.json", "methodology", true);
    }

    @DisplayName("Incorrect file")
    @Test
    public void testIncorrectFile() {
        executeControllerTest("medicalClaim_incorrectFile.json", "file", false);
    }

    @DisplayName("Incorrect file type")
    @Test
    public void testIncorrectFileType() {
        executeControllerTest("meg9.34.json", "file", false);
    }

    @DisplayName("Missing required field in file type rule")
    @Test
    public void testMissingRequiredField_fileType() {
        executeControllerTest("missingRequiredField_fileType.json", "file", false);
    }

    @DisplayName("Missing required field in methodology type rule")
    @Test
    public void testMissingRequiredField_methodologyType() {
        executeControllerTest("missingRequiredField_methodologyType.json", "methodology", false);
    }

    @DisplayName("Invalid type in presence field")
    @Test
    public void testInvalidType_presenceField() {
        executeControllerTest("invalidPresenceField.json", "methodology", false);
    }

    @DisplayName("Additional fields in file type json")
    @Test
    public void testAdditionalField_fileType() {
        executeControllerTest("additionalField_fileType.json", "file", false);
    }

    @DisplayName("Additional fields in methodology type json")
    @Test
    public void testAdditionalField_methodologyType() {
        executeControllerTest("additionalField_methodologyType.json", "methodology", false);
    }

    private void executeControllerTest(String ruleFileName, String type, boolean expectedResult) {
        String rule = TestUtil.loadFile(ruleFileName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(rule, headers);
        ValidationReport report = restTemplate.postForObject(
            "http://localhost:" + port + "/rule/validate/" + type,
            entity, ValidationReport.class);
        TestUtil.assertValidationReport(report, expectedResult);
    }

}
