package com.velychko.rvs.validation.helper;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;


import com.fasterxml.jackson.databind.JsonNode;
import com.velychko.rvs.validation.model.Rule;
import com.velychko.rvs.validation.util.TestUtil;
import java.io.InputStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


public class RuleValidationHelperTest {

    @DisplayName("Test success load schema file from resources")
    @Test
    public void testLoadSchema() {
        InputStream is = RuleValidationHelper.loadValidationSchema(Rule.METHODOLOGY);

        assertNotNull(is, "Input stream should be not null");
    }

    @DisplayName("Test rule type null")
    @Test
    public void testNullRuleType() {
        assertThrows(IllegalArgumentException.class,
            () -> RuleValidationHelper.loadValidationSchema(null),
            "Method should thrown IllegalArgumentException when rule type is equal to null!");
    }

    @DisplayName("Test rule string null")
    @Test
    public void testNullRuleString() {
        assertThrows(IllegalArgumentException.class,
            () -> RuleValidationHelper.createJsonNode((String) null),
            "Method should thrown IllegalArgumentException when string with rule equal to null!");
    }

    @DisplayName("Test null schema InputStream")
    @Test
    public void testNullSchemaInputStream() {
        assertThrows(IllegalArgumentException.class,
            () -> RuleValidationHelper.createJsonNode((InputStream) null),
            "Method should thrown IllegalArgumentException when InputStream equal to null!" );
    }

    @DisplayName("Test not throw on valid json")
    @Test
    public void testSuccessCreateJsonNode() throws Exception {
        Assertions.assertDoesNotThrow(
            () -> RuleValidationHelper.createJsonNode(TestUtil.loadFile("meg9.34.json")),
            "Method shouldn't thrown any exception on valid json!");
    }

    @DisplayName("Test success converting")
    @Test
    public void testSuccessConverting() throws Exception {
        String rule = TestUtil.loadFile("meg9.34.json");
        JsonNode jsonNode = RuleValidationHelper.createJsonNode(rule);

        assertNotNull(jsonNode, "JsonNode should be not null!");
    }
}
