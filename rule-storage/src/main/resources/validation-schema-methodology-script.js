db.runCommand({
    collMod: "methodologies",
    validator: {
        $jsonSchema: {
            title : "Methodology rules schema",
            bsonType : "object",
            properties: {
                _id: {},
                _class: {},
                file : {
                    bsonType : "string"
                },
                methodology : {
                    bsonType: "string"
                },
                fields : {
                    bsonType: "array",
                    minItems: 1,
                    items: {
                        bsonType: "object",
                        properties: {
                            name : {
                                bsonType: "string"
                            },
                            presence: {
                                enum: ["O", "HM", "VM"]
                            }
                        },
                        additionalProperties: false,
                        required: ["name", "presence"]
                    }
                }
            },
            additionalProperties: false,
            required: ["file", "methodology", "fields"]
        },
    },
    validationAction: "error"
})