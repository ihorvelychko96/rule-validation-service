db.runCommand({
    collMod: "fields",
    validator: {
        $jsonSchema: {
            title: "Field validation schema",
            bsonType: "object",
            properties: {
                _id: {},
                _class: {},
                file: {
                    bsonType: "string"
                },
                name: {
                    bsonType: "string"
                },
                errorCode: {
                    bsonType: "string"
                },
                valueType: {
                    bsonType: "string"
                },
                valueFormat: {
                    bsonType: "string"
                },
                derivedBy: {
                    bsonType: "array"
                },
                rules: {
                    bsonType: "array",
                    minItems: 1,
                    items: {
                        bsonType: "object",
                        properties: {
                            methodology: {
                                bsonType: "string"
                            },
                            presence: {
                                enum: ["O", "HM", "VM"]
                            }
                        },
                        additionalProperties: false,
                        required: ["methodology", "presence"]
                    }
                }
            },
            additionalProperties: false,
            required: ["file", "name", "errorCode", "valueType", "rules"]

        }
    }
})