db.runCommand({
  collMod: "files",
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["file", "fields"],
      description: "must be an object and is required",
      properties: {
        _id: {},
        _class: {},
        file : {
          bsonType : "string",
          description: "must be a string and is required",
          unique: true
        },
        fields : {
          bsonType: "array",
          minItems: 1,
          items: {
            bsonType: "object",
            required: ["name", "valueType"],
            properties: {
              name : {
                bsonType: "string",
                description: "must be a string and is required"
              },
              valueType: {
                bsonType: "string",
                description: "must be a string and is required"
              },
              valueFormat: {
                bsonType: "string",
                description: "must be a string"
              },
              derivedBy: {
                bsonType: "array",
                description: "must be an array"
              }
            },
            additionalItems: false,
          }
        }
      },
      additionalProperties: false,
    }
  },
  validationAction: "error"
})