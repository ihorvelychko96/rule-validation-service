package com.velychko.rvs.storage.model.compiled;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CompiledFieldDto {

    private String id;
    private String name;
    private String errorCode;
    private String valueType;
    private String valueFormat;
    private String[] derivedBy;
    private Set<CompiledRule> rules;


    public CompiledFieldDto clone() {
        Set<CompiledRule> copyOfCompiledRule = rules.stream().map(CompiledRule::clone).collect(Collectors.toSet());
        return new CompiledFieldDto(this.id,this.name, this.errorCode, this.valueType,
            this.valueFormat, this.derivedBy, copyOfCompiledRule);
    }
}
