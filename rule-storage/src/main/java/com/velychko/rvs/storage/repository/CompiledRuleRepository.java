package com.velychko.rvs.storage.repository;

import com.velychko.rvs.storage.model.compiled.CompiledField;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompiledRuleRepository extends MongoRepository<CompiledField, String> {
    Optional<CompiledField> findCompiledFieldByNameAndFile(String fieldName, String fileName);
    List<CompiledField> findCompiledFieldsByFile(String file);
    List<CompiledField> deleteCompiledFieldByNameAndFile(String fieldName, String fileName);
}
