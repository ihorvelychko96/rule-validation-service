package com.velychko.rvs.storage.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.velychko.rvs.storage.model.field.FileRuleField;
import java.util.Set;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("files")
@Data
public class FileRule {

    @Id
    private String id;
    @Field("file")
    @JsonProperty("file")
    private String fileName;
    private Set<FileRuleField> fields;
}
