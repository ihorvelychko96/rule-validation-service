package com.velychko.rvs.storage.repository;

import com.velychko.rvs.storage.model.FileRule;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRuleRepository extends MongoRepository<FileRule, String> {
    Optional<FileRule> findFileRuleByFileName(String fileName);
    List<FileRule> removeFileRuleByFileName(String fileName);
}
