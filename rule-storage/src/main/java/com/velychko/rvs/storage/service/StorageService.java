package com.velychko.rvs.storage.service;

import com.velychko.rvs.storage.model.compiled.CompiledFileRuleDto;
import com.velychko.rvs.storage.model.FileRule;
import com.velychko.rvs.storage.model.MethodologyRule;
import com.velychko.rvs.storage.model.compiled.CompiledField;
import java.util.List;
import java.util.Optional;

public interface StorageService {
    Optional<FileRule> saveFileRule(FileRule rule);
    Optional<MethodologyRule> saveMethodologyRule(MethodologyRule rule);

    Optional<FileRule> findFileRule(String fileName);
    Optional<MethodologyRule> findMethodologyRule(String fileName, String methodologyName);

    List<FileRule> findAllFileRules();
    List<MethodologyRule> findAllMethodologyRules();
    List<MethodologyRule> findAllMethodologyRulesByFileName(String fileName);
    List<MethodologyRule> findAllMethodologyRulesByMethodology(String methodologyName);

    Optional<CompiledFileRuleDto> getCompiledFileRule(String fileName);
    Optional<CompiledField> findCompiledField(String fieldName, String fileName);
    List<CompiledField> findCompiledFieldByFile(String file);
    Optional<CompiledField> saveCompiledField(CompiledField compiledField);
    Optional<CompiledField> updateCompiledField(CompiledField compiledField);
    List<CompiledField> deleteCompiledField(String fieldName, String fileName);

    Optional<FileRule> updateFileRule(FileRule rule);
    Optional<MethodologyRule> updateMethodologyRule(MethodologyRule rule);

    List<FileRule> deleteFileRule(String fileName);
    List<MethodologyRule> deleteMethodologyRule(String fileName, String methodologyName);
}
