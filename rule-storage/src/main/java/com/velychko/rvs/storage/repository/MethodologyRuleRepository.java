package com.velychko.rvs.storage.repository;

import com.velychko.rvs.storage.model.MethodologyRule;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MethodologyRuleRepository extends MongoRepository<MethodologyRule, String> {
    Optional<MethodologyRule> findMethodologyRuleByFileNameAndMethodology(String fileName, String methodologyName);
    List<MethodologyRule> findMethodologyRulesByFileName(String fileName);
    List<MethodologyRule> findMethodologyRulesByMethodology(String methodologyName);
    List<MethodologyRule> removeMethodologyRuleByFileNameAndMethodology(String fileName, String methodologyName);

}
