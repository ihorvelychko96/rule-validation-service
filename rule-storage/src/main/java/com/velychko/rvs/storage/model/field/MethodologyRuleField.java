package com.velychko.rvs.storage.model.field;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MethodologyRuleField {

    @EqualsAndHashCode.Include
    private String name;
    private String presence;
}
