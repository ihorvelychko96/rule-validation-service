package com.velychko.rvs.storage.model.compiled;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CompiledRule {

    @EqualsAndHashCode.Include
    @NotBlank
    private String methodology;
    @NotBlank
    private String presence;

    public CompiledRule clone() {
        return new CompiledRule(this.methodology, this.presence);
    }
}
