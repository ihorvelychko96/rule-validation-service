package com.velychko.rvs.storage.service.impl;

import com.velychko.rvs.storage.model.compiled.CompiledFieldDto;
import com.velychko.rvs.storage.model.compiled.CompiledFileRuleDto;
import com.velychko.rvs.storage.model.FileRule;
import com.velychko.rvs.storage.model.MethodologyRule;
import com.velychko.rvs.storage.model.compiled.CompiledField;
import com.velychko.rvs.storage.repository.CompiledRuleRepository;
import com.velychko.rvs.storage.repository.FileRuleRepository;
import com.velychko.rvs.storage.repository.MethodologyRuleRepository;
import com.velychko.rvs.storage.service.StorageService;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StorageServiceImpl implements StorageService {

    private FileRuleRepository fileRuleRepository;
    private MethodologyRuleRepository methodologyRuleRepository;
    private CompiledRuleRepository compiledRuleRepository;

    public StorageServiceImpl(FileRuleRepository fileRuleRepository,
                              MethodologyRuleRepository methodologyRuleRepository,
                              CompiledRuleRepository compiledRuleRepository) {
        this.fileRuleRepository = fileRuleRepository;
        this.methodologyRuleRepository = methodologyRuleRepository;
        this.compiledRuleRepository = compiledRuleRepository;
    }

    @Override
    public Optional<FileRule> saveFileRule(FileRule rule) {
        if (!fileRuleRepository.findFileRuleByFileName(rule.getFileName()).isPresent()) {
            log.info("Saving new file rule {}", rule.toString());
            return Optional.of(fileRuleRepository.save(rule));
        } else {
            log.info("Rule already exist in database");
            return Optional.empty();
        }
    }

    @Override
    public Optional<MethodologyRule> saveMethodologyRule(MethodologyRule rule) {
        if (!methodologyRuleRepository.findMethodologyRuleByFileNameAndMethodology(rule.getFileName(), rule.getMethodology()).isPresent()) {
            log.info("Saving new methodology rule {}", rule.toString());
            return Optional.of(methodologyRuleRepository.save(rule));
        } else {
            log.info("Rule already exist in database");
            return Optional.empty();
        }
    }

    @Override
    public Optional<FileRule> findFileRule(String fileName) {
        return fileRuleRepository.findFileRuleByFileName(fileName);
    }

    @Override
    public Optional<MethodologyRule> findMethodologyRule(String fileName, String methodologyName) {
        return methodologyRuleRepository.findMethodologyRuleByFileNameAndMethodology(fileName, methodologyName);
    }

    @Override
    public List<FileRule> findAllFileRules() {
        return fileRuleRepository.findAll();
    }

    @Override
    public List<MethodologyRule> findAllMethodologyRules() {
        return methodologyRuleRepository.findAll();
    }

    @Override
    public List<MethodologyRule> findAllMethodologyRulesByFileName(String fileName) {
        return methodologyRuleRepository.findMethodologyRulesByFileName(fileName);
    }

    @Override
    public List<MethodologyRule> findAllMethodologyRulesByMethodology(String methodologyName) {
        return methodologyRuleRepository.findMethodologyRulesByMethodology(methodologyName);
    }

    @Override
    public Optional<CompiledFileRuleDto> getCompiledFileRule(String fileName) {
        List<CompiledField> fields = compiledRuleRepository.findCompiledFieldsByFile(fileName);

        if (CollectionUtils.isNotEmpty(fields)) {
            
            return Optional.of(convertCompiledFieldToCompiledFileRuleDto(fileName, fields));
        } else {
            return Optional.empty();
        }
    }

    private CompiledFileRuleDto convertCompiledFieldToCompiledFileRuleDto(String fileName, List<CompiledField> fields) {
        CompiledFileRuleDto compiledFileRule = new CompiledFileRuleDto();
        compiledFileRule.setFileName(fileName);

        ModelMapper mapper = new ModelMapper();

        fields.forEach(field -> {
            CompiledFieldDto fieldDto = mapper.map(field, CompiledFieldDto.class);
            compiledFileRule.addField(fieldDto);
        });
        return compiledFileRule;
    }

    @Override
    public Optional<CompiledField> findCompiledField(String fieldName, String fileName) {
        return compiledRuleRepository.findCompiledFieldByNameAndFile(fieldName, fileName);
    }

    @Override
    public List<CompiledField> findCompiledFieldByFile(String file) {
        return compiledRuleRepository.findCompiledFieldsByFile(file);
    }

    @Override
    public Optional<CompiledField> saveCompiledField(CompiledField compiledField) {
        if (!compiledRuleRepository.findCompiledFieldByNameAndFile(compiledField.getName(), compiledField.getFile()).isPresent()) {
            log.info("Saving new compiled field {}", compiledField);
            return Optional.of(compiledRuleRepository.save(compiledField));
        } else {
            log.info("Compiled field with given field name and file already exist {}", compiledField);
            return Optional.empty();
        }
    }

    @Override
    public Optional<CompiledField> updateCompiledField(CompiledField compiledField) {
        if (compiledRuleRepository.findById(compiledField.getId()).isPresent()) {
            log.info("Updating compiled field {}", compiledField);
            return Optional.of(compiledRuleRepository.save(compiledField));
        } else {
            log.info("Updated compiled rule is not present in storage {}", compiledField);
            return Optional.empty();
        }

    }

    @Override
    public List<CompiledField> deleteCompiledField(String fieldName, String fileName) {
        return compiledRuleRepository.deleteCompiledFieldByNameAndFile(fieldName, fileName);
    }

    @Override
    public Optional<FileRule> updateFileRule(FileRule rule) {
        if (fileRuleRepository.findById(rule.getId()).isPresent()) {
            log.info("Updating file rule {}", rule.toString());
            return Optional.of(fileRuleRepository.save(rule));
        } else {
            log.info("Updated file rule is not present in storage {}", rule);
            return Optional.empty();
        }
    }

    @Override
    public Optional<MethodologyRule> updateMethodologyRule(MethodologyRule rule) {
        if (methodologyRuleRepository.findById(rule.getId()).isPresent()) {
            log.info("Updating methodology rule {}", rule.toString());
            return Optional.of(methodologyRuleRepository.save(rule));
        } else {
            log.info("Updated methodology rule is not present in storage {}", rule);
            return Optional.empty();
        }
    }

    @Override
    public List<FileRule> deleteFileRule(String fileName) {
        return fileRuleRepository.removeFileRuleByFileName(fileName);
    }

    @Override
    public List<MethodologyRule> deleteMethodologyRule(String fileName, String methodologyName) {
        return methodologyRuleRepository.removeMethodologyRuleByFileNameAndMethodology(fileName, methodologyName);
    }
}
