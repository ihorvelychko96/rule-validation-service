package com.velychko.rvs.storage.controller;

import com.mongodb.MongoException;
import com.mongodb.MongoWriteException;
import com.velychko.rvs.storage.model.FileRule;
import com.velychko.rvs.storage.model.MethodologyRule;
import com.velychko.rvs.storage.model.compiled.CompiledField;
import com.velychko.rvs.storage.model.compiled.CompiledFileRuleDto;
import com.velychko.rvs.storage.service.StorageService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("rule")
public class StorageController {

    private StorageService service;

    StorageController(StorageService service) {
        this.service = service;
    }

    @PostMapping(value = "storage/file/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FileRule> createFileRule(@RequestBody FileRule rule) {
        return service.saveFileRule(rule)
            .map(fileRule -> new ResponseEntity<>(fileRule, HttpStatus.CREATED))
            .orElse(new ResponseEntity<>(HttpStatus.CONFLICT));
    }

    @PostMapping(value = "storage/methodology/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MethodologyRule> createMethodologyRule(@RequestBody MethodologyRule rule) {
        return service.saveMethodologyRule(rule)
            .map(methodologyRule -> new ResponseEntity<>(methodologyRule, HttpStatus.CREATED))
            .orElse(new ResponseEntity<>(HttpStatus.CONFLICT));
    }

    @GetMapping(value = "storage/file/{fileName}")
    public ResponseEntity<FileRule> getFileRule(@PathVariable("fileName") String fileName) {
        return service.findFileRule(fileName)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping(value = "storage/methodology/{fileName}/{methodologyName}")
    public ResponseEntity<MethodologyRule> getMethodologyRule(
        @PathVariable("fileName") String fileName,
        @PathVariable("methodologyName") String methodologyName) {

        return service.findMethodologyRule(fileName, methodologyName)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @GetMapping(value = "storage/file")
    public ResponseEntity<List<FileRule>> getAllFileRules() {
        List<FileRule> rules = service.findAllFileRules();
        return CollectionUtils.isNotEmpty(rules)
            ? ResponseEntity.ok(rules)
            : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "storage/methodology")
    public ResponseEntity<List<MethodologyRule>> getAllMethodologyRules() {
        List<MethodologyRule> rules = service.findAllMethodologyRules();
        return CollectionUtils.isNotEmpty(rules)
            ? ResponseEntity.ok(rules)
            : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "storage/methodology/file/{fileName}")
    public ResponseEntity<List<MethodologyRule>> getAllMethodologyRulesByFileName(@PathVariable("fileName") String fileName) {
        List<MethodologyRule> rules = service.findAllMethodologyRulesByFileName(fileName);
        return CollectionUtils.isNotEmpty(rules)
            ? ResponseEntity.ok(rules)
            : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "storage/methodology/{methodologyName}")
    public ResponseEntity<List<MethodologyRule>> getAllMethodologyRulesMethodology(@PathVariable("methodologyName") String methodology) {
        List<MethodologyRule> rules = service.findAllMethodologyRulesByMethodology(methodology);
        return CollectionUtils.isNotEmpty(rules)
            ? ResponseEntity.ok(rules)
            : new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "storage/compiled/{fileName}/{fieldName}")
    public ResponseEntity<CompiledField> getCompiledField(@PathVariable("fieldName") String fieldName,
                                                          @PathVariable("fileName") String fileName) {
        return service.findCompiledField(fieldName, fileName)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = "storage/compiled/{fileName}")
    public ResponseEntity<CompiledFileRuleDto> getCompiledFieldByFile(@PathVariable("fileName") String fileName) {
        return service.getCompiledFileRule(fileName)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = "storage/compiled/create")
    public ResponseEntity<CompiledField> createCompiledField(@RequestBody @Valid CompiledField compiledField) {
        return service.saveCompiledField(compiledField)
            .map(field -> new ResponseEntity<>(field, HttpStatus.CREATED))
            .orElse(new ResponseEntity<>(HttpStatus.CONFLICT));
    }

    @PutMapping(value = "storage/compiled/update")
    public ResponseEntity<CompiledField> updateCompiledField(@RequestBody CompiledField compiledField) {
        return service.updateCompiledField(compiledField)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping(value = "storage/compiled/delete/{fileName}/{fieldName}")
    public ResponseEntity<List<CompiledField>> deleteCompiledField(@PathVariable("fieldName") String fieldName,
                                                                   @PathVariable("fileName") String fileName) {
        List<CompiledField> fields = service.deleteCompiledField(fieldName, fileName);
        return CollectionUtils.isNotEmpty(fields)
            ? new ResponseEntity<>(fields, HttpStatus.OK)
            : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "storage/file/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FileRule> updateFileRule(@RequestBody FileRule rule) {
        return service.updateFileRule(rule)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @PutMapping(value = "storage/methodology/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MethodologyRule> updateMethodologyRule(@RequestBody MethodologyRule rule) {
        return service.updateMethodologyRule(rule)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @DeleteMapping(value = "storage/file/delete/{fileName}")
    public ResponseEntity<?> deleteFileRule(@PathVariable("fileName") String fileName) {
        List<FileRule> rules = service.deleteFileRule(fileName);
        return CollectionUtils.isNotEmpty(rules)
            ? new ResponseEntity<>(rules, HttpStatus.OK)
            : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "storage/methodology/delete/{fileName}/{methodologyName}")
    public ResponseEntity<?> deleteMethodologyRule(@PathVariable("fileName") String fileName,
                                            @PathVariable("methodologyName") String methodologyName) {
        List<MethodologyRule> rules = service.deleteMethodologyRule(fileName, methodologyName);
        return CollectionUtils.isNotEmpty(rules)
            ? new ResponseEntity<>(rules, HttpStatus.OK)
            : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Exception handler for validation errors.
     *
     * @param response
     * @param ex
     * @throws IOException
     */
    @ExceptionHandler(MongoWriteException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void handleMongoException(HttpServletResponse response, MongoException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

}
