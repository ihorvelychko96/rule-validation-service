package com.velychko.rvs.storage.model.compiled;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class CompiledFileRuleDto {

    @JsonProperty("file")
    private String fileName;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Set<CompiledFieldDto> fields = new HashSet<>();

    public void addField(CompiledFieldDto field) {
        fields.add(field.clone());
    }

    public Set<CompiledFieldDto> getFields() {
        Set<CompiledFieldDto> destSet = new HashSet<>();
        fields.forEach(field -> {
            destSet.add(field.clone());
        });
        return destSet;
    }
}
