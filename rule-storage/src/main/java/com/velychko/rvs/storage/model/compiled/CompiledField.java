package com.velychko.rvs.storage.model.compiled;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.HashSet;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "fields")
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class CompiledField {

    private String id;
    @EqualsAndHashCode.Include
    @NotBlank
    private String file;
    @EqualsAndHashCode.Include
    @NotBlank
    private String name;
    @NotBlank
    private String errorCode;
    @NotBlank
    private String valueType;
    private String valueFormat;
    private String[] derivedBy;
    @NotEmpty
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private Set<@Valid CompiledRule> rules = new HashSet<>();

    public void addRule(CompiledRule rule) {
        rules.add(rule.clone());
    }

    public Set<CompiledRule> getRules() {
        Set<CompiledRule> destSet = new HashSet<>();
        this.rules.forEach(rule -> {
            destSet.add(rule.clone());
        });
        return destSet;
    }
}
