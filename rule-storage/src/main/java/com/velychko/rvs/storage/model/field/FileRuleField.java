package com.velychko.rvs.storage.model.field;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class FileRuleField {

    @EqualsAndHashCode.Include
    private String name;

    private String valueType;
    private String valueFormat;
    private String[] derivedBy;
}
