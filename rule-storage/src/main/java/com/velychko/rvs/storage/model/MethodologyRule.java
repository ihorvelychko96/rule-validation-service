package com.velychko.rvs.storage.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.velychko.rvs.storage.model.field.MethodologyRuleField;
import java.util.Set;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document("methodologies")
@Data
public class MethodologyRule {

    @Id
    private String id;
    @Field("file")
    @JsonProperty("file")
    private String fileName;
    private String methodology;
    private Set<MethodologyRuleField> fields;
}
