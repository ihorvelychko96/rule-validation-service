package com.velychko.rvs.storage.service;

import static org.junit.jupiter.api.Assertions.*;


import com.velychko.rvs.storage.model.compiled.CompiledField;
import com.velychko.rvs.storage.model.compiled.CompiledFileRuleDto;
import com.velychko.rvs.storage.model.FileRule;
import com.velychko.rvs.storage.model.MethodologyRule;
import com.velychko.rvs.storage.repository.CompiledRuleRepository;
import com.velychko.rvs.storage.repository.FileRuleRepository;
import com.velychko.rvs.storage.repository.MethodologyRuleRepository;
import com.velychko.rvs.storage.service.impl.StorageServiceImpl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StorageServiceTest {

    @Injectable private FileRuleRepository fileRuleRepository;
    @Injectable private MethodologyRuleRepository methodologyRuleRepository;
    @Injectable private CompiledRuleRepository compiledRuleRepository;
    @Tested private StorageServiceImpl service;

    @BeforeEach
    public void init() {
        service = new StorageServiceImpl(fileRuleRepository, methodologyRuleRepository, compiledRuleRepository);
    }

    @DisplayName("Test save new file rule when given rule already exist")
    @Test
    public void testSaveAlreadyExistFileRule(@Mocked FileRule ruleObject) {
        new Expectations() {{
            fileRuleRepository.findFileRuleByFileName(anyString); result = ruleObject;
        }};

        Optional<FileRule> result = service.saveFileRule(ruleObject);

        assertFalse(result.isPresent(), "Result should be null");
    }

    @DisplayName("Test save success new file rule")
    @Test
    public void testSuccessSaveFileRule(@Mocked FileRule ruleObject) {
        Optional<FileRule> fileRuleOptional = Optional.empty();
        new Expectations() {{
            fileRuleRepository.findFileRuleByFileName(anyString); result = fileRuleOptional;
        }};

        Optional<FileRule> result = service.saveFileRule(ruleObject);

        assertTrue(result.isPresent(), "Result should be persisted object");
    }

    @DisplayName("Test save new methodology rule when given rule already exist")
    @Test
    public void testSaveAlreadyExistMethodologyRule(@Mocked MethodologyRule methodologyRule) {
        new Expectations() {{
            methodologyRuleRepository.findMethodologyRuleByFileNameAndMethodology(anyString, anyString); result = methodologyRule;
        }};

        Optional<MethodologyRule> result = service.saveMethodologyRule(methodologyRule);

        assertFalse(result.isPresent(), "Result should be null");
    }

    @DisplayName("Test save success new methodology rule")
    @Test
    public void testSuccessSaveMethodologyRule(@Mocked MethodologyRule methodologyRule) {
        Optional<MethodologyRule> methodologyRuleOptional = Optional.empty();
        new Expectations() {{
            methodologyRuleRepository.findMethodologyRuleByFileNameAndMethodology(anyString, anyString);
            result = methodologyRuleOptional;
        }};

        Optional<MethodologyRule> result = service.saveMethodologyRule(methodologyRule);

        assertTrue(result.isPresent(), "Result should be persisted object");
    }

    @DisplayName("Test find file rule which isn't exist.")
    @Test
    public void testFindFileRuleNotExist() {
        Optional<FileRule> fileRuleOptional = Optional.empty();
        new Expectations() {{
            fileRuleRepository.findFileRuleByFileName(anyString); result = fileRuleOptional;
        }};

        Optional<FileRule> result = service.findFileRule("Name_Of_Not_Exist_Rule");

        assertFalse(result.isPresent(), "Optional should be empty");
    }

    @DisplayName("Test find file rule.")
    @Test
    public void testFindFileRule(@Mocked FileRule rule) {
        new Expectations() {{
            fileRuleRepository.findFileRuleByFileName(anyString); result = rule;
        }};

        Optional<FileRule> result = service.findFileRule("Rule_Name");

        assertTrue(result.isPresent(), "Optional should contain rule");
    }

    @DisplayName("Test find methodology rule which isn't exist.")
    @Test
    public void testFindMethodologyRuleNotExist() {
        Optional<MethodologyRule> methodologyRuleOptional = Optional.empty();

        new Expectations() {{
            methodologyRuleRepository.findMethodologyRuleByFileNameAndMethodology(anyString, anyString);
            result = methodologyRuleOptional;
        }};

        Optional<MethodologyRule> result = service.findMethodologyRule("file", "methodology");

        assertFalse(result.isPresent(), "Optional should be empty");
    }

    @DisplayName("Test find methodology rule.")
    @Test
    public void testFindMethodologyRule(@Mocked MethodologyRule rule) {
        new Expectations() {{
            methodologyRuleRepository.findMethodologyRuleByFileNameAndMethodology(anyString, anyString); result = rule;
        }};

        Optional<MethodologyRule> result = service.findMethodologyRule("fileName", "methodlogy");

        assertTrue(result.isPresent(), "Optional should contain rule");
    }

    @DisplayName("Test find all methodology rule which isn't exist.")
    @Test
    public void testFindAllMethodologyRulesNotExist() {
        List<MethodologyRule> rules = new ArrayList<>();
        new Expectations() {{
            methodologyRuleRepository.findAll(); result = rules;
        }};

        List<MethodologyRule> result = service.findAllMethodologyRules();

        assertAll(
            () -> assertNotNull(result, "List should not be null"),
            () -> assertEquals(0, result.size(), "List should be empty")
        );
    }

    @DisplayName("Test find all methodology rules")
    @Test
    public void testFindAllMethodologyRules() {
        MethodologyRule[] ruleArray = {new MethodologyRule(), new MethodologyRule()};
        List<MethodologyRule> rules = new ArrayList<>(Arrays.asList(ruleArray));

        new Expectations() {{
            methodologyRuleRepository.findAll(); result = rules;
        }};

        List<MethodologyRule> result = service.findAllMethodologyRules();

        assertAll(
            () -> assertNotNull(result, "List should not be null"),
            () -> assertNotEquals(0, result.size(), "List should not be empty")
        );
    }

    @DisplayName("Test find all file rule which isn't exist.")
    @Test
    public void testFindAllFileRulesNotExist() {
        List<FileRule> rules = new ArrayList<>();

        new Expectations() {{
            fileRuleRepository.findAll(); result = rules;
        }};

        List<FileRule> result = service.findAllFileRules();

        assertAll(
            () -> assertNotNull(result, "List should not be null"),
            () -> assertEquals(0, result.size(), "List should be empty")
        );
    }

    @DisplayName("Test find all file rules")
    @Test
    public void testFindAllFileRules() {
        FileRule[] ruleArray = { new FileRule(), new FileRule() };
        List<FileRule> rules = new ArrayList<>(Arrays.asList(ruleArray));

        new Expectations() {{
            fileRuleRepository.findAll(); result = rules;
        }};

        List<FileRule> result = service.findAllFileRules();

        assertAll(
            () -> assertNotNull(result, "List should not be null"),
            () -> assertNotEquals(0, result.size(), "List should not be empty")
        );
    }

    @DisplayName("Test find all methodology rules by file name")
    @Test
    public void testFindAllMethodologyRulesByFileName() {
        MethodologyRule[] ruleArray = {new MethodologyRule(), new MethodologyRule()};
        List<MethodologyRule> rules = new ArrayList<>(Arrays.asList(ruleArray));

        new Expectations() {{
            methodologyRuleRepository.findMethodologyRulesByFileName(anyString); result = rules;
        }};

        List<MethodologyRule> result = service.findAllMethodologyRulesByFileName("file");

        assertAll(
            () -> assertNotNull(result, "List should not be null"),
            () -> assertNotEquals(0, result.size(), "List should not be empty")
        );
    }

    @DisplayName("Test find all methodology rules by methodology name")
    @Test
    public void testFindAllMethodologyRulesByMethodologyName() {
        MethodologyRule[] ruleArray = {new MethodologyRule(), new MethodologyRule()};
        List<MethodologyRule> rules = new ArrayList<>(Arrays.asList(ruleArray));

        new Expectations() {{
            methodologyRuleRepository.findMethodologyRulesByMethodology(anyString); result = rules;
        }};

        List<MethodologyRule> result = service.findAllMethodologyRulesByMethodology("file");

        assertAll(
            () -> assertNotNull(result, "List should not be null"),
            () -> assertNotEquals(0, result.size(), "List should not be empty")
        );
    }

    @DisplayName("Test success update file rule")
    @Test
    public void testSuccessUpdateFileRule(@Mocked FileRule fileRule) {

        new Expectations() {{
            fileRuleRepository.findById(anyString); result = fileRule;
        }};

        Optional<FileRule> updatedRule = service.updateFileRule(fileRule);

        assertTrue(updatedRule.isPresent(), "File rule should be present");
    }

    @DisplayName("Test fail update file rule")
    @Test
    public void testFailUpdateFileRule(@Mocked FileRule fileRule) {
        Optional<FileRule> fileRuleOptional = Optional.empty();
        new Expectations() {{
            fileRuleRepository.findById(anyString); result = fileRuleOptional;
        }};

        Optional<FileRule> updatedRule = service.updateFileRule(fileRule);

        assertFalse(updatedRule.isPresent(), "File rule should not be present");
    }

    @DisplayName("Test success update methodology rule")
    @Test
    public void testSuccessUpdateMethodologyRule(@Mocked MethodologyRule methodologyRule) {

        new Expectations() {{
            methodologyRuleRepository.findById(anyString); result = methodologyRule;
        }};

        Optional<MethodologyRule> updatedRule = service.updateMethodologyRule(methodologyRule);

        assertTrue(updatedRule.isPresent(), "Methodology rule should be present");
    }

    @DisplayName("Test fail update methodology rule")
    @Test
    public void testFailUpdateMethodologyRule(@Mocked MethodologyRule methodologyRule) {
        Optional<MethodologyRule> methodologyRuleOptional = Optional.empty();

        new Expectations() {{
            methodologyRuleRepository.findById(anyString);
            result = methodologyRuleOptional;
        }};

        Optional<MethodologyRule> updatedRule = service.updateMethodologyRule(methodologyRule);

        assertFalse(updatedRule.isPresent(), "Methodology rule should not be present");
    }

    @DisplayName("Test success delete file rule")
    @Test
    public void testSuccessDeleteFileRule(@Mocked FileRule fileRule) {
        List<FileRule> rules = Arrays.asList(fileRule);
        new Expectations() {{
            fileRuleRepository.removeFileRuleByFileName(anyString);
            result = rules;
        }};

        List<FileRule> rule = service.deleteFileRule("FileName");

        assertFalse(rule.isEmpty(), "Rule should be deleted");
    }

    @DisplayName("Test fail delete file rule")
    @Test
    public void testFailDeleteFileRule() {
        List<FileRule> rules = Collections.emptyList();
        new Expectations() {{
            fileRuleRepository.removeFileRuleByFileName(anyString);
            result = rules;
        }};

        List<FileRule> rule = service.deleteFileRule("FileName");

        assertTrue(rule.isEmpty(), "Rule should not be deleted");
    }

    @DisplayName("Test success delete methodology rule")
    @Test
    public void testSuccessDeleteMethodologyRule(@Mocked MethodologyRule methodologyRule) {
        List<MethodologyRule> rules = Arrays.asList(methodologyRule);
        new Expectations() {{
            methodologyRuleRepository.removeMethodologyRuleByFileNameAndMethodology(anyString, anyString);
            result = rules;
        }};

        List<MethodologyRule> rule = service.deleteMethodologyRule("FileName", "methodologyRule");

        assertFalse(rule.isEmpty(), "Rule should be deleted");
    }

    @DisplayName("Test fail delete methodology rule")
    @Test
    public void testFailDeleteMethodologyRule() {
        List<MethodologyRule> rules = Collections.emptyList();
        new Expectations() {{
            methodologyRuleRepository.removeMethodologyRuleByFileNameAndMethodology(anyString, anyString);
            result = rules;
        }};

        List<MethodologyRule> rule = service.deleteMethodologyRule("FileName", "methodologyRule");

        assertTrue(rule.isEmpty(), "Rule should not be deleted");
    }

    @DisplayName("Test success create compiled field ")
    @Test
    public void testSuccessCreateField(@Mocked CompiledField field) {
        Optional<CompiledField> fieldOptional = Optional.empty();

        new Expectations() {{
            compiledRuleRepository.findCompiledFieldByNameAndFile(anyString, anyString);
            result = fieldOptional;
            compiledRuleRepository.save(field); result = field;
        }};

        Optional<CompiledField> persistedFieldOptional = service.saveCompiledField(field);

        assertTrue(persistedFieldOptional.isPresent(), "Field should be present");
    }

    @DisplayName("Test fail create compiled field")
    @Test
    public void testFailCreateCompiledField(@Mocked CompiledField field) {
        Optional<CompiledField> fieldOptional = Optional.of(new CompiledField());
        new Expectations() {{
            compiledRuleRepository.findCompiledFieldByNameAndFile(anyString, anyString);
            result = fieldOptional;
        }};

        Optional<CompiledField> persistedFieldOptional = service.saveCompiledField(field);

        assertFalse(persistedFieldOptional.isPresent(), "Field should not be present");
    }

    @DisplayName("Test success update field")
    @Test
    public void testSuccessUpdateCompiledField(@Mocked CompiledField field) {
        Optional<CompiledField> fieldOptional = Optional.of(new CompiledField());

        new Expectations() {{
            compiledRuleRepository.findById(anyString);
            result = fieldOptional;
            compiledRuleRepository.save(field); result = field;
        }};

        Optional<CompiledField> updatedFieldOptional = service.updateCompiledField(field);

        assertTrue(updatedFieldOptional.isPresent(), "Field should be present");
    }

    @DisplayName("Test fail update compiled field")
    @Test
    public void testFailUpdateCompiledField(@Mocked CompiledField field) {
        Optional<CompiledField> fieldOptional = Optional.empty();

        new Expectations() {{
            compiledRuleRepository.findById(anyString); result = fieldOptional;
        }};

        Optional<CompiledField> updatedFieldOptional = service.updateCompiledField(field);

        assertFalse(updatedFieldOptional.isPresent(), "Field should not be present");
    }

    @DisplayName("Test success delete compiled field")
    @Test
    public void testSuccessDeleteCompiledField() {
        List<CompiledField> fields = Collections.singletonList(new CompiledField());

        new Expectations() {{
            compiledRuleRepository.deleteCompiledFieldByNameAndFile(anyString, anyString);
            result = fields;
        }};

        List<CompiledField> deletedField = service.deleteCompiledField("fieldName", "fileName");

        assertFalse(deletedField.isEmpty(), "List of fields should not be empty");
    }

    @DisplayName("Test fail delete compiled field")
    @Test
    public void testFailDeleteCompiledField() {
        List<CompiledField> fields = Collections.emptyList();

        new Expectations() {{
            compiledRuleRepository.deleteCompiledFieldByNameAndFile(anyString, anyString);
            result = fields;
        }};

        List<CompiledField> deletedField = service.deleteCompiledField("fieldName", "fileName");

        assertTrue(deletedField.isEmpty(), "List of fields should be empty");
    }

    @DisplayName("Test success generation compiled rule")
    @Test
    public void testSuccessGenerationCompiledRule() {
        List<CompiledField> fields = Collections.singletonList(new CompiledField());

        new Expectations() {{
            service.findCompiledFieldByFile(anyString); result = fields;
        }};

        Optional<CompiledFileRuleDto> compiledFileRule =  service.getCompiledFileRule("file");

        assertTrue(compiledFileRule.isPresent(), "Compiled rule should be present");

    }

    @DisplayName("Test fail generation compiled rule")
    @Test
    public void testFailGenerationCompiledFileRule() {
        List<CompiledField> fields = Collections.emptyList();
        new Expectations() {{
            service.findCompiledFieldByFile(anyString); result = fields;
        }};

        Optional<CompiledFileRuleDto> compiledFileRule =  service.getCompiledFileRule("file");

        assertFalse(compiledFileRule.isPresent(), "Compiled rule should be present");

    }
}
