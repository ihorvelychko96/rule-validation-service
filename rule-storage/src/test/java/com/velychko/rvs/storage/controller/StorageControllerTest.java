package com.velychko.rvs.storage.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.velychko.rvs.storage.model.FileRule;
import com.velychko.rvs.storage.model.MethodologyRule;
import com.velychko.rvs.storage.service.StorageService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(StorageController.class)
public class StorageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private StorageService service;

    @DisplayName("Test find file rule")
    @Test
    public void testFindFileRule() throws Exception {
        FileRule rule = new FileRule();
        Optional<FileRule> ruleOptional = Optional.of(rule);

        when(service.findFileRule("medicalClaim")).thenReturn(ruleOptional);
        mockMvc.perform(get("/rule/storage/file/medicalClaim"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isNotEmpty())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @DisplayName("Test find file rule is not exist")
    @Test
    public void testFindNotExistFileRule() throws Exception {
        Optional<FileRule> ruleOptional = Optional.empty();

        when(service.findFileRule("notExistRule")).thenReturn(ruleOptional);

        mockMvc.perform(get("/rule/storage/file/notExistRule"))
            .andExpect(jsonPath("$").doesNotExist())
            .andExpect(status().isNoContent());
    }

    @DisplayName("Test find methodology rule")
    @Test
    public void testFindMethodologyRule() throws Exception {
        Optional<MethodologyRule> ruleOptional = Optional.of(new MethodologyRule());

        when(service.findMethodologyRule("fileName", "methodologyName"))
            .thenReturn(ruleOptional);

        mockMvc.perform(get("/rule/storage/methodology/fileName/methodologyName"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test find methodology rule is not exist")
    @Test
    public void testFindNotExistMethodologyRule() throws Exception {
        Optional<MethodologyRule> ruleOptional = Optional.empty();

        when(service.findMethodologyRule("notExistRule", "notExist")).thenReturn(ruleOptional);

        mockMvc.perform(get("/rule/storage/methodology/notExistRule/notExist"))
            .andExpect(jsonPath("$").doesNotExist())
            .andExpect(status().isNoContent());
    }

    @DisplayName("Test find all FileRules")
    @Test
    public void testFindAllFileRule() throws Exception {
        List<FileRule> rules = new ArrayList<>(Collections.singletonList(new FileRule()));

        when(service.findAllFileRules()).thenReturn(rules);

        mockMvc.perform(get("/rule/storage/file"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test find all FileRules when rules not exist")
    @Test
    public void testFindAllFileRulesWhenRulesIsNotExist() throws Exception {
        List<FileRule> rules = Collections.emptyList();

        when(service.findAllFileRules()).thenReturn(rules);

        mockMvc.perform(get("/rule/storage/file"))
            .andExpect(jsonPath("$").doesNotExist())
            .andExpect(status().isNoContent());
    }

    @DisplayName("Test find all MethodologyRule")
    @Test
    public void testFindAllMethodologyRules() throws Exception {
        List<MethodologyRule> rules = Collections.singletonList(new MethodologyRule());

        when(service.findAllMethodologyRules()).thenReturn(rules);

        mockMvc.perform(get("/rule/storage/methodology"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test find all MethodologyRules when rules not exist")
    @Test
    public void testFindAllMethodologyRulesWhenRulesIsNotExist() throws Exception {
        List<MethodologyRule> rules = Collections.emptyList();

        when(service.findAllMethodologyRules()).thenReturn(rules);

        mockMvc.perform(get("/rule/storage/methodology"))
            .andExpect(jsonPath("$").doesNotExist())
            .andExpect(status().isNoContent());
    }

    @DisplayName("Test find all MethodologyRules by file name")
    @Test
    public void testFindAllMethodologyRulesByFileName() throws Exception {
        List<MethodologyRule> rules = Collections.singletonList(new MethodologyRule());

        when(service.findAllMethodologyRulesByFileName("fileName"))
            .thenReturn(rules);

        mockMvc.perform(get("/rule/storage/methodology/file/fileName"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test find all MethodologyRules by file name when rules is not exist")
    @Test
    public void testFindAllMethodologyRulesByFileNameWhenRulesIsNotExist() throws Exception {
        List<MethodologyRule> rules = Collections.emptyList();

        when(service.findAllMethodologyRulesByFileName("fileName"))
            .thenReturn(rules);

        mockMvc.perform(get("/rule/storage/methodology/file/fileName"))
            .andExpect(status().isNoContent())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @DisplayName("Test find all MethodologyRules by methodology name")
    @Test
    public void testFindAllMethodologyRulesByMethodology() throws Exception {
        List<MethodologyRule> rules = Collections.singletonList(new MethodologyRule());

        when(service.findAllMethodologyRulesByMethodology("methodName"))
            .thenReturn(rules);

        mockMvc.perform(get("/rule/storage/methodology/methodName"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test find all MethodologyRules by methodology name when rules is not exist")
    @Test
    public void testFindAllMethodologyRulesByMethodologyWhenRuleIsNotExist() throws  Exception {
        List<MethodologyRule> rules = Collections.emptyList();

        when(service.findAllMethodologyRulesByMethodology("methodName"))
            .thenReturn(rules);

        mockMvc.perform(get("/rule/storage/methodology/methodName"))
            .andExpect(status().isNoContent())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @DisplayName("Test create file rule")
    @Test
    public void testCreateFileRule() throws Exception {
        FileRule fileRule = new FileRule();
        Optional<FileRule> ruleOptional = Optional.of(fileRule);
        when(service.saveFileRule(fileRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(fileRule);

        mockMvc.perform(post("/rule/storage/file/create")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isCreated())
            .andExpect(jsonPath("$").isNotEmpty())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
    }

    @DisplayName("Test fail create file rule")
    @Test
    public void testFailCreateFileRule() throws Exception {
        FileRule fileRule = new FileRule();
        Optional<FileRule> ruleOptional = Optional.empty();
        when(service.saveFileRule(fileRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(fileRule);

        mockMvc.perform(post("/rule/storage/file/create")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @DisplayName("Test create methodology rule")
    @Test
    public void testCreateMethodologyRule() throws Exception {
        MethodologyRule methodologyRule = new MethodologyRule();
        Optional<MethodologyRule> ruleOptional = Optional.of(methodologyRule);

        when(service.saveMethodologyRule(methodologyRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(methodologyRule);

        mockMvc.perform(post("/rule/storage/methodology/create")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isCreated())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test fail create methodology rule")
    @Test
    public void testFailCreateMethodologyRule() throws Exception {
        MethodologyRule methodologyRule = new MethodologyRule();
        Optional<MethodologyRule> ruleOptional = Optional.empty();

        when(service.saveMethodologyRule(methodologyRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(methodologyRule);

        mockMvc.perform(post("/rule/storage/methodology/create")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isConflict())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @DisplayName("Test update file rule")
    @Test
    public void testUpdateFileRule() throws Exception {
        FileRule fileRule = new FileRule();
        Optional<FileRule> ruleOptional = Optional.of(fileRule);

        when(service.updateFileRule(fileRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(fileRule);

        mockMvc.perform(put("/rule/storage/file/update")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test fail update file rule")
    @Test
    public void testFailUpdateFileRule() throws Exception {
        FileRule fileRule = new FileRule();
        Optional<FileRule> ruleOptional = Optional.empty();

        when(service.updateFileRule(fileRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(fileRule);

        mockMvc.perform(put("/rule/storage/file/update")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isNoContent())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @DisplayName("Test update methodologyRule")
    @Test
    public void testUpdateMethodologyRule() throws Exception {
        MethodologyRule methodologyRule = new MethodologyRule();
        Optional<MethodologyRule> ruleOptional = Optional.of(methodologyRule);

        when(service.updateMethodologyRule(methodologyRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(methodologyRule);

        mockMvc.perform(put("/rule/storage/methodology/update")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test fail update methodologyRule")
    @Test
    public void testFailUpdateMethodologyRule() throws Exception {
        MethodologyRule methodologyRule = new MethodologyRule();
        Optional<MethodologyRule> ruleOptional = Optional.empty();

        when(service.updateMethodologyRule(methodologyRule))
            .thenReturn(ruleOptional);

        String json = objectMapper.writeValueAsString(methodologyRule);

        mockMvc.perform(put("/rule/storage/methodology/update")
            .contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isNoContent())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @DisplayName("Test delete file rule")
    @Test
    public void testDeleteFileRule() throws Exception {
        List<FileRule> rules = Collections.singletonList(new FileRule());

        when(service.deleteFileRule("ruleName"))
            .thenReturn(rules);

        mockMvc.perform(delete("/rule/storage/file/delete/ruleName"))
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test fail delete file rule")
    @Test
    public void testFailDeleteFileRule() throws Exception {
        List<FileRule> rules = Collections.emptyList();

        when(service.deleteFileRule("ruleName"))
            .thenReturn(rules);

        mockMvc.perform(delete("/rule/storage/file/delete/ruleName"))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$").doesNotExist());
    }

    @DisplayName("Test delete methodology rule")
    @Test
    public void testDeleteMethodologyRule() throws Exception {
        List<MethodologyRule> rules = Collections.singletonList(new MethodologyRule());

        when(service.deleteMethodologyRule("file", "methodology"))
            .thenReturn(rules);

        mockMvc.perform(delete("/rule/storage/methodology/delete/file/methodology"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isNotEmpty());
    }

    @DisplayName("Test fail delete methodology rule")
    @Test
    public void testFailDeleteMethodologyRule() throws Exception {
        List<MethodologyRule> rules = Collections.emptyList();

        when(service.deleteMethodologyRule("file", "methodology"))
            .thenReturn(rules);

        mockMvc.perform(delete("/rule/storage/delete/file/methodology"))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$").doesNotExist());
    }
}
