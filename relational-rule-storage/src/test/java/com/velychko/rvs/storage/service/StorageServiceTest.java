package com.velychko.rvs.storage.service;

import static org.junit.jupiter.api.Assertions.*;


import com.velychko.rvs.storage.model.CompiledField;
import com.velychko.rvs.storage.model.CompiledFieldDto;
import com.velychko.rvs.storage.model.File;
import com.velychko.rvs.storage.repository.CompiledFieldRepository;
import com.velychko.rvs.storage.repository.FileRepository;
import com.velychko.rvs.storage.service.impl.StorageServiceImpl;
import java.util.Optional;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Mocked;
import mockit.Tested;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StorageServiceTest {

    @Tested private StorageServiceImpl service;
    @Injectable private CompiledFieldRepository compiledFieldRepository;
    @Injectable private FileRepository fileRepository;

    @BeforeEach
    public void init() {
        this.service = new StorageServiceImpl(compiledFieldRepository, fileRepository);
    }

    @DisplayName("Test successful - create CompiledField")
    @Test
    public void testSuccessfulCreateCompiledField(@Mocked CompiledField field, @Mocked File fileType) {
        Optional<CompiledField> fieldOptional = Optional.empty();
        Optional<File> fileOptional = Optional.of(fileType);

        new Expectations() {{
            fileRepository.findFileByName(anyString); result = fileOptional;
            compiledFieldRepository.findCompiledFieldByFileIdAndName(anyLong, anyString);
            result = fieldOptional;
        }};

        Optional<CompiledField> createdField = service.saveCompiledField(new CompiledFieldDto());

        assertTrue(createdField.isPresent(), "CompiledField should be present");
    }

    @DisplayName("Test fail - create CompiledField, when field already exists")
    @Test
    public void testFailCreateCompiledFieldWhenFieldAlreadyExists(@Mocked CompiledField field, @Mocked File fileType) {
        Optional<CompiledField> fieldOptional = Optional.of(new CompiledField());
        Optional<File> fileOptional = Optional.of(fileType);

        new Expectations() {{
            fileRepository.findFileByName(anyString); result = fileOptional;
            compiledFieldRepository.findCompiledFieldByFileIdAndName(anyLong, anyString);
            result = fieldOptional;
        }};

        Optional<CompiledField> createdField = service.saveCompiledField(new CompiledFieldDto());

        assertFalse(createdField.isPresent(), "CompiledField should not be present");
    }

    @DisplayName("Test fail - create CompiledField, when file type does not exist")
    @Test
    public void testFailCreateCompiledFieldWhenFileTypeDoesNotExist() {
        Optional<File> fileOptional = Optional.empty();

        new Expectations() {{
            fileRepository.findFileByName(anyString); result = fileOptional;
        }};

        Optional<CompiledField> createdField = service.saveCompiledField(new CompiledFieldDto());

        assertFalse(createdField.isPresent(), "CompiledField should not be present");
    }

    @DisplayName("Test successful - create File")
    @Test
    public void testSuccessfulCreateFile() {
        Optional<File> fileOptional = Optional.empty();

        new Expectations() {{
            fileRepository.findFileByName(anyString); result = fileOptional;
        }};

        Optional<File> createdFile = service.saveFileType(new File());

        assertTrue(createdFile.isPresent(), "File should be present");
    }

    @DisplayName("Test fail - create File, when file type already exists")
    @Test
    public void testFailCreateFileWhenFileAlreadyExists() {
        Optional<File> fileOptional = Optional.of(new File());

        new Expectations() {{
            fileRepository.findFileByName(anyString); result = fileOptional;
        }};

        Optional<File> createdFile = service.saveFileType(new File());

        assertFalse(createdFile.isPresent(), "File should not be present");
    }
}
