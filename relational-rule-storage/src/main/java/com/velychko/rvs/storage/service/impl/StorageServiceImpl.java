package com.velychko.rvs.storage.service.impl;

import com.velychko.rvs.storage.model.CompiledField;
import com.velychko.rvs.storage.model.CompiledFieldDto;
import com.velychko.rvs.storage.model.File;
import com.velychko.rvs.storage.repository.CompiledFieldRepository;
import com.velychko.rvs.storage.repository.FileRepository;
import com.velychko.rvs.storage.service.StorageService;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class StorageServiceImpl implements StorageService {

    private CompiledFieldRepository compiledFieldRepository;
    private FileRepository fileRepository;
    private ModelMapper mapper;

    public StorageServiceImpl(CompiledFieldRepository compiledFieldRepository, FileRepository fileRepository) {
        this.compiledFieldRepository = compiledFieldRepository;
        this.fileRepository = fileRepository;
        this.mapper = new ModelMapper();
    }

    @Override
    public Optional<CompiledField> saveCompiledField(CompiledFieldDto field) {
        Optional<File> fileType = fileRepository.findFileByName(field.getFile());

        if (fileType.isPresent() && !compiledFieldRepository.findCompiledFieldByFileIdAndName(fileType.get().getId(), field.getName()).isPresent()) {
            CompiledField compiledField = mapper.map(field, CompiledField.class);
            compiledField.setFile(fileType.get());
            compiledField.getRules().forEach(rule -> {
                rule.setField(compiledField);
            });
            return Optional.of(compiledFieldRepository.save(compiledField));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<File> saveFileType(File fileType) {
        if (!fileRepository.findFileByName(fileType.getName()).isPresent()) {
            return Optional.of(fileRepository.save(fileType));
        } else {
            return Optional.empty();
        }
    }
}
