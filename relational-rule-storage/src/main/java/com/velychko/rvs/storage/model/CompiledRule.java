package com.velychko.rvs.storage.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@Entity
@Table(name = "rules", uniqueConstraints = {@UniqueConstraint(columnNames = {"methodology", "field_id"})})
public class CompiledRule {

    @Id
    @SequenceGenerator(name = "ruleIdSequenceGenerator", sequenceName = "RULE_SEQUENCE", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ruleIdSequenceGenerator")
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String methodology;

    @NotBlank
    @Column(nullable = false)
    private String presence;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "field_id", referencedColumnName = "id")
    @JsonIgnore
    private CompiledField field;
}
