package com.velychko.rvs.storage.controller;

import com.velychko.rvs.storage.model.CompiledField;
import com.velychko.rvs.storage.model.CompiledFieldDto;
import com.velychko.rvs.storage.model.File;
import com.velychko.rvs.storage.service.StorageService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StorageController {

    private StorageService service;

    StorageController(StorageService service) {
        this.service = service;
    }

    @PostMapping(value = "storage/compiled/field/create")
    public ResponseEntity<CompiledField> createCompiledField(@RequestBody @Valid CompiledFieldDto field) {
        return service.saveCompiledField(field)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.CONFLICT));
    }

    @PostMapping(value = "storage/compiled/file/create")
    public ResponseEntity<File> createFileType(@RequestBody @Valid File fileType) {
        return service.saveFileType(fileType)
            .map(ResponseEntity::ok)
            .orElse(new ResponseEntity<>(HttpStatus.CONFLICT));
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleConstraintViolationException(HttpServletResponse response, DataIntegrityViolationException exception) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), exception.getMessage());
    }
}
