package com.velychko.rvs.storage.service;

import com.velychko.rvs.storage.model.CompiledField;
import com.velychko.rvs.storage.model.CompiledFieldDto;
import com.velychko.rvs.storage.model.File;
import java.util.Optional;

public interface StorageService {
    Optional<CompiledField> saveCompiledField(CompiledFieldDto field);
    Optional<File> saveFileType(File fileType);
}
