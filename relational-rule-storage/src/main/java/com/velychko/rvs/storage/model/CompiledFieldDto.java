package com.velychko.rvs.storage.model;

import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CompiledFieldDto {

    @NotBlank
    private String file;

    @NotBlank
    private String name;

    @NotNull
    private Boolean nullable;

    @NotBlank
    private String valueType;

    private String valueFormat;

    @NotEmpty
    private Set<@Valid CompiledRuleDto> rules;
}
