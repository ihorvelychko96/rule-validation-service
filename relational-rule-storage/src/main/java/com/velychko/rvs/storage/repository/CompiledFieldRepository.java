package com.velychko.rvs.storage.repository;

import com.velychko.rvs.storage.model.CompiledField;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompiledFieldRepository extends JpaRepository<CompiledField, Long> {
    Optional<CompiledField> findCompiledFieldByFileIdAndName(Long fileId, String name);
}
