package com.velychko.rvs.storage.repository;

import com.velychko.rvs.storage.model.File;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<File, Long> {
    Optional<File> findFileByName(String name);
}
