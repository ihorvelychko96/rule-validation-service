package com.velychko.rvs.storage.model;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CompiledRuleDto {

    @NotBlank
    private String methodology;

    @NotBlank
    private String presence;
}
