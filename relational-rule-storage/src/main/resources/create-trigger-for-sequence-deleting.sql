drop trigger if exists on_delete_sequence_trigger on public.files cascade;

create or replace function delete_unique_sequence() returns trigger as '
    DECLARE
        file_name VARCHAR(50);
    BEGIN
        SELECT files.name INTO file_name FROM files WHERE files.id = OLD.id;
        EXECUTE format(''DROP SEQUENCE IF EXISTS %s_file_type_sequence'', file_name);
        RETURN OLD;
    END;
    ' language plpgsql;

create trigger on_delete_sequence_trigger
    before delete
    on public.files for each row
execute procedure delete_unique_sequence();