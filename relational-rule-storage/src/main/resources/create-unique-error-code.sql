drop trigger if exists fields_unique_error_code_trigger on public.fields cascade;

create or replace function set_error_code() returns trigger as '
DECLARE
    file_name VARCHAR(50);
    sequence_name VARCHAR(50);
BEGIN
    SELECT files.name INTO file_name FROM files WHERE files.id = NEW.file_id;
    sequence_name := format(''%s_file_type_sequence'', file_name);
    NEW.error_code := nextval(sequence_name);
    RETURN NEW;
END;
' language plpgsql;

create trigger fields_unique_error_code_trigger
    before insert
    on public.fields for each row
execute procedure set_error_code();