drop trigger if exists sequence_trigger on public.files cascade;

create or replace function create_sequence() returns trigger as '
    DECLARE
        file_name VARCHAR(50) := NEW.name;
    BEGIN
        EXECUTE format(''create sequence if not exists %s_file_type_sequence minvalue 0'', file_name);
        RETURN NEW;
    END;
    ' language  plpgsql;

create trigger sequence_trigger
    after insert
    on public.files for each row
execute procedure create_sequence();